package main

import (
	"bytes"
	"flag"
	"io/ioutil"
	"log"

	"github.com/rwcarlsen/gallery/backend"
	"github.com/rwcarlsen/gallery/backend/amz"
	"github.com/rwcarlsen/goamz/aws"
)

func main() {
	flag.Parse()
	srcPath := flag.Arg(0)
	dstPath := flag.Arg(1)

	data, err := ioutil.ReadFile(srcPath)
	if err != nil {
		log.Fatal(err)
	}

	db := amzLib()
	if err := db.Put(dstPath, bytes.NewReader(data)); err != nil {
		log.Fatal(err)
	}
	log.Println("success")
}

func amzLib() backend.Interface {
	auth, err := aws.EnvAuth()
	if err != nil {
		log.Fatal(err)
	}
	db := amz.New(auth, aws.USEast)
	db.DbName = "amz"
	return db
}
